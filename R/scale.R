library(graphics)
data.tbl<-read.csv('str_feature', header=FALSE)
data.tbl[,c(3:nrow(data.tbl))]<-scale(data.tbl[,c(3:nrow(data.tbl))])
write.table(feature.tbl, file = "str_feature.scale",sep=",", col.names=FALSE)
