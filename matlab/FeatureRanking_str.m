%%% 1d Feature Ranking
%%% This plots roc curves for each feature individually

function [aucVals]=FeatureRanking1d(x,y,fig)

colorsAre=['r','m','k','g','c','y','k','r','m','y','c','k',...
           'g','g','g','g','g','g',...
           'y','y','y',...
           'b','b','b','k','k','k',...
           'm','m','m',...
           'c','c','c','c','c','c','c','c','c','y'];

figure(fig)
clf
for feature=1:size(x,2)
  
  %[aucvalue,rocunnorm,junk]=rocCurvesPseudo(x(:,feature),y);
  [aucvalue,rocunnorm]=rocCurvesAndValues(x(:,feature),y);
  aucVals(feature)=aucvalue;
  figure(fig)
  if feature~=1
   hold on
  end
  color=colorsAre(feature);
  h=plot(rocunnorm,color);
  set(h,'LineWidth',2);
  set(gca, 'Fontsize',20);
  width=2;
  set(0,'DefaultAxesLineWidth',width);
  set(0,'DefaultLineLineWidth',width);
  get(0,'Default');
  set(gca,'LineWidth', width);
  h = get(gca,'children');
end
hold on

%legend('Logistic\_Reg Combine', 'Logistic\_Reg Structure', 'Logistic\_Reg Month', 'Logistic\_Reg Combine 2009', 'Logistic\_Reg Structure 2009', 'Logistic\_Reg Combine 2008', 'Logistic\_Reg Structure 2008', 'Logistic\_Reg Combine 1999', 'Logistic\_Reg Structure 1999');
%legend('Logistic\_Reg Combine', 'Logistic\_Reg Structure', 'Logistic\_Reg Month', 'Logistic\_Reg Combine 2009', 'Logistic\_Reg Structure 2009', 'Logistic\_Reg Combine 2008', 'Logistic\_Reg Structure 2008', 'Logistic\_Reg Combine 1999', 'Logistic\_Reg Structure 1999');
leg=legend('Logistic\_Reg Weather and Month ID (AUC=0.6)', 'Logistic\_Reg Month ID (AUC=0.577)', 'Logistic\_Reg Structure Only (AUC=0.53)', 'Logistic\_Reg Structure with Bias (AUC=0.501)');
%leg=legend('1999','2000','2001', '2002', '2003', '2004', '2005', '2006', '2007', '2008', '2009');
%legend('Logistic\_Reg Combine');
%xlim([0 2633])
%ylim([0 2666])
set(leg,'Location','SouthEast')
%pseudo roc
%plot([1,length(y)],[0,length(find(y==1))],'-');
%standard roc
plot([1,length(find(y==-1))],[0,length(find(y==1))],'-');

aucVals
%randAuc=length(find(y==1))*length(find(y==-1))/2

