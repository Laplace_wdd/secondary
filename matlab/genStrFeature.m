function genStrFeature(year)
cablefeatures=dlmread('txt_1/ColumbiaCableFeatures_ME_Fall_2011NoLabels.txt');
eventfeatures=dlmread(['txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_' num2str(year) 'NoLabels.txt']);
omfeatures=dlmread(['txt_1/ColumbiaConsolidatedTable_OM_Features_ME_input_year_' num2str(year) 'NoLabels.txt']);
%graphfeatures=dlmread(['txt_1/StructureGraphFeatures_input_year_' num2str(year) 'NoLabels.txt']);
graphfeatures=dlmread(['txt_1/StructureGraphFeatures_input_year_2008NoLabels.txt']);

mynocablesets=find(cablefeatures(:,end-3)==0); % it is the same as mynocables
hascables = setdiff(1:size(cablefeatures,1),mynocablesets);
Dataset = [graphfeatures(hascables,[7,8,9,10]),sum(graphfeatures(hascables,[12,13])')'>0,omfeatures(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures(hascables,[2,3,5,9,11,15,17,26,27])];
str_ids=cablefeatures(hascables,[1]);

fid=fopen(['txt_1/str_feature' num2str(year) '.csv'],'w');
mymat=[str_ids Dataset];
for i =[1:size(mymat,1)]
	fprintf(fid,['%10i,' num2str(year)],mymat(i,1)); 
	for j = [2:size(mymat,2)]
		fprintf(fid,',%f',mymat(i,j)); 
	end
	fprintf(fid,'\n'); 
end
fclose(fid);
