cablefeatures=dlmread('txt_1/ColumbiaCableFeatures_ME_Fall_2011NoLabels.txt');
eventfeatures2010=dlmread('txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_2010NoLabels.txt');
eventfeatures2011=dlmread('txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_2011NoLabels.txt');
omfeatures2010=dlmread('txt_1/ColumbiaConsolidatedTable_OM_Features_ME_input_year_2010NoLabels.txt');
omfeatures2011=dlmread('txt_1/ColumbiaConsolidatedTable_OM_Features_ME_input_year_2011NoLabels.txt');
graphfeatures2010=dlmread('txt_1/StructureGraphFeatures_input_year_2010NoLabels.txt');
graphfeatures2011=dlmread('txt_1/StructureGraphFeatures_input_year_2011NoLabels.txt');

label2010=eventfeatures2010(:,end);
label2011=eventfeatures2011(:,end);

mynocables=find(cablefeatures(:,end)==0);
mynocablesets=find(cablefeatures(:,end-3)==0); % it is the same as mynocables
hascables = setdiff(1:size(cablefeatures,1),mynocablesets);
str_ids=cablefeatures(hascables,[1]);

Dataset2010 = [graphfeatures2010(hascables,[7,8,9,10]),sum(graphfeatures2010(hascables,[12,13])')'>0,omfeatures2010(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures2010(hascables,[2,3,5,9,11,15,17,26,27,28])]; % label is last column

Dataset2011 = [graphfeatures2011(hascables,[7,8,9,10]),sum(graphfeatures2011(hascables,[12,13])')'>0,omfeatures2011(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures2011(hascables,[2,3,5,9,11,15,17,26,27,28])]; % label is last column

fid=fopen('txt_1/str_feature2009.csv','w');
mymat=[str_ids Dataset2010];
for i =[1:size(mymat,1)]
	fprintf(fid,'%10i,2009',mymat(i,1)); 
	for j = [2:size(mymat,2)-1]
		fprintf(fid,',%f',mymat(i,j)); 
	end
	fprintf(fid,'\n'); 
end
fclose(fid);

fid=fopen('txt_1/str_feature2010.csv','w');
mymat=[str_ids Dataset2011];
for i =[1:size(mymat,1)]
	fprintf(fid,'%10i,2010',mymat(i,1)); 
	for j = [2:size(mymat,2)-1]
		fprintf(fid,',%f',mymat(i,j)); 
	end
	fprintf(fid,'\n'); 
end
fclose(fid);
