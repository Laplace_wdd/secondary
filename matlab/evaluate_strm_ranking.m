filename = 'rank_lst_month.csv';
fid = fopen(filename,'r');  % Open csv file
data = textscan(fid,'%f %f %f','HeaderLines',1,'Delimiter',',');
fclose(fid);

x = [data{2} data{3}];
y = data{1};
fig = 1;
[aucVals]=FeatureRanking_strm(x,y,fig);
aucVals / (length(find(y==1)) * length(find(y==-1)))
%x = [data{5}];
%[dcg,mrr,aucval,pnormval2,pnormval4,pnormval8,pnormval16,pnormval64]=MeasuresOfQuality(x,y)
%dcg
