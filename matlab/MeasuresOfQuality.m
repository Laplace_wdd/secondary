function [dcg,mrr,aucval,pnormval2,pnormval4,pnormval8,pnormval16,pnormval64]=MeasuresOfQuality(f,y)

%%% it is important that y and f are column vectors - should print an error otherwise

[fsort,indices]=sort(f);
ysort=(y(indices));
%ysort=(y(indices(1:5000)));

%%%% get dcg, mrr, auc
dcg=sum(1./log(1+find(flipud(ysort)==1) ));
mrr=sum(1./find(flipud(ysort)==1));
[tp,fp]=cawleyroc(y,f);
%[tp,fp]=cawleyroc(y(indices(1:5000)),fsort(1:5000));
aucval=cawleyauroc(tp,fp);

%%% get pnorm vals
scoresYpos=(find(ysort==1))';
scoresYneg=(find(ysort==-1))';

for ibar=1:length(scoresYneg)
 normvalK(ibar)=length(find(scoresYpos <= scoresYneg(ibar)));
end

pnormval1=sum(normvalK.^1);
 aucshouldbe = 1- pnormval1/(length(scoresYneg)*length(scoresYpos));
pnormval2=sum(normvalK.^2);
pnormval4=sum(normvalK.^4);
pnormval8=sum(normvalK.^8);
pnormval16=sum(normvalK.^16);
pnormval64=sum(normvalK.^64);















if(0)   %%%% some fake data to test it on
f=[1:5]';
y=[-1,-1,1,-1,1]';
f=rand(15,1);
y=sign(round(rand(15,1))-.5);
end

if (0)

scoresYpos=flipud(find(yTrain==1))';
scoresYneg=flipud(find(yTrain==-1))';

for ibar=1:length(scoresYpos)
 prec(ibar)=length(find(scoresYpos >= scoresYpos(ibar))) / (length(find(scoresYpos >= scoresYpos(ibar)))+length(find(scoresYneg >= scoresYpos(ibar))));
end



scoresYpos = [15,10,6,4,2,1];
scoresYneg=[14,13,9,8,7,3,0];

for ibar=1:length(scoresYpos)
 prec(ibar)=length(find(scoresYpos >= scoresYpos(ibar))) / (length(find(scoresYpos >= scoresYpos(ibar)))+length(find(scoresYneg >= scoresYpos(ibar))));
end


end
