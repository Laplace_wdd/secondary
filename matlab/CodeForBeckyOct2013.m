cablefeatures=dlmread('txt_1/ColumbiaCableFeatures_ME_Fall_2011NoLabels.txt');
eventfeatures2008=dlmread('txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_2008NoLabels.txt');
eventfeatures2009=dlmread('txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_2009NoLabels.txt');
eventfeatures2010=dlmread('txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_2010NoLabels.txt');
eventfeatures2011=dlmread('txt_1/ColumbiaConsolidatedTable_ECS_Features_ME_input_year_2011NoLabels.txt');
omfeatures2008=dlmread('txt_2/ColumbiaConsolidatedTable_OM_Features_ME_input_year_2008NoLabels.txt');
omfeatures2009=dlmread('txt_2/ColumbiaConsolidatedTable_OM_Features_ME_input_year_2009NoLabels.txt');
omfeatures2010=dlmread('txt_2/ColumbiaConsolidatedTable_OM_Features_ME_input_year_2010NoLabels.txt');
omfeatures2011=dlmread('txt_2/ColumbiaConsolidatedTable_OM_Features_ME_input_year_2011NoLabels.txt');
graphfeatures2008=dlmread('txt_3/StructureGraphFeatures_input_year_2008NoLabels.txt');
graphfeatures2009=dlmread('txt_3/StructureGraphFeatures_input_year_2009NoLabels.txt');
graphfeatures2010=dlmread('txt_3/StructureGraphFeatures_input_year_2010NoLabels.txt');
graphfeatures2011=dlmread('txt_3/StructureGraphFeatures_input_year_2011NoLabels.txt');
svdfeatures2008=dlmread('txt_4/svdfeatures2008_208NoLabels.txt');
svdfeatures2009=dlmread('txt_4/svdfeatures2009_208NoLabels.txt');
svdfeatures2010=dlmread('txt_4/svdfeatures2010_208NoLabels.txt');
svdfeatures2011=dlmread('txt_4/svdfeatures2011_208NoLabels.txt');
coverinformation=dlmread('txt_5/ColumbiaStructures_SB_MH_Network_Cover_ME_Fall_2011NoLabels.txt');


label2008=eventfeatures2008(:,end);
label2009=eventfeatures2009(:,end);
label2010=eventfeatures2010(:,end);
label2011=eventfeatures2011(:,end);

mynocables=find(cablefeatures(:,end)==0);
mynocablesets=find(cablefeatures(:,end-3)==0); % it is the same as mynocables
hascables = setdiff(1:size(cablefeatures,1),mynocablesets);

mystructureids=cablefeatures(hascables,1);

mystructureidsnocables=cablefeatures(mynocablesets,1);

fid=fopen('ForBeckyJuly172013_nocables.txt','w')
mymat=[mystructureidsnocables];
fprintf(fid,'%10i,%f\n',mymat'); 
fclose(fid)


Dataset2008 = [graphfeatures2008(hascables,[7,8,9,10]),sum(graphfeatures2008(hascables,[12,13])')'>0,omfeatures2008(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures2008(hascables,[2,3,5,9,11,15,17,26,27,28])]; % label is last column

Dataset2009 = [graphfeatures2009(hascables,[7,8,9,10]),sum(graphfeatures2009(hascables,[12,13])')'>0,omfeatures2009(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures2009(hascables,[2,3,5,9,11,15,17,26,27,28])]; % label is last column

Dataset2010 = [graphfeatures2010(hascables,[7,8,9,10]),sum(graphfeatures2010(hascables,[12,13])')'>0,omfeatures2010(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures2010(hascables,[2,3,5,9,11,15,17,26,27,28])]; % label is last column

Dataset2011 = [graphfeatures2011(hascables,[7,8,9,10]),sum(graphfeatures2011(hascables,[12,13])')'>0,omfeatures2011(hascables,[2,42]),cablefeatures(hascables,[2,8,31,40,51,57]),cablefeatures(hascables,31)>0,cablefeatures(hascables,60)>0,eventfeatures2011(hascables,[2,3,5,9,11,15,17,26,27,28])]; % label is last column



X_Train=Dataset2010(:,1:end-1);
Y_Train=Dataset2010(:,end);
X_Test=Dataset2011(:,1:end-1);
Y_Test=Dataset2011(:,end);

%----

B = glmfit(X_Train, Y_Train, 'binomial', 'link', 'logit');

Ftr=[ones(size(X_Train,1),1) X_Train]*B;
FeatureRanking1d(F,Y_Train,14);

Fte=[ones(size(X_Test,1),1) X_Test]*B;
FeatureRanking1d(F,Y_Test,15);



fid=fopen('TestingRankedList.txt','w')
mymat=[mystructureids, Fte];
fprintf(fid,'%10i,%f\n',mymat'); 
fclose(fid)
