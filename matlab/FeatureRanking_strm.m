%%% 1d Feature Ranking
%%% This plots roc curves for each feature individually

function [aucVals]=FeatureRanking1d(x,y,fig)

colorsAre=['r','m','k','g','c','y','k','r','m','y','c','k',...
           'g','g','g','g','g','g',...
           'y','y','y',...
           'b','b','b','k','k','k',...
           'm','m','m',...
           'c','c','c','c','c','c','c','c','c','y'];

figure(fig)
clf
for feature=1:size(x,2)
  
  %[aucvalue,rocunnorm,junk]=rocCurvesPseudo(x(:,feature),y);
  [aucvalue,rocunnorm]=rocCurvesAndValues(x(:,feature),y);
  aucVals(feature)=aucvalue;
  figure(fig)
  if feature~=1
   hold on
  end
  color=colorsAre(feature);
  h=plot(rocunnorm,color);
  set(h,'LineWidth',2);
  set(gca, 'Fontsize',20);
  width=2;
  set(0,'DefaultAxesLineWidth',width);
  set(0,'DefaultLineLineWidth',width);
  get(0,'Default');
  set(gca,'LineWidth', width);
  h = get(gca,'children');
end
hold on

leg=legend('Logistic\_Reg Combine', 'Logistic\_Reg Structure');
set(leg,'Location','SouthEast')
%xlim([0 2633])
%ylim([0 2666])

%pseudo roc
%plot([1,length(y)],[0,length(find(y==1))],'-');
%standard roc
plot([1,length(find(y==-1))],[0,length(find(y==1))],'-');

aucVals
%randAuc=length(find(y==1))*length(find(y==-1))/2

