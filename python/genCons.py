import sys, re, string, pickle
from itertools import *

def main():
	inCsv = open(sys.argv[1], 'r')
	outCsv = open("../csv/month_model/nlti3l.csv", 'w')
	genEveCnt(inCsv, outCsv)
	inCsv.close()
	outCsv.close()

def readDay(inCsv):
	oldYear, curCon, conLst = '', 0, [0]
	for l in inCsv:
		entry = l.strip().split(",")
		curYear = entry[0]+","+entry[1]
		curEve = float(entry[6])
		if oldYear == '':
			oldYear = curYear
		if curYear == oldYear:
			if curEve > -50 and curEve < 50: 
				curCon += 1
			elif curCon != 0:
				conLst += [curCon]
				curCon = 0
		else:
			if curCon != 0:
				conLst += [curCon]
			resLst = [str(i) for i in conLst if i >= 1]
			resRec = '{0},{1},{2},{3}'.format(oldYear, str(max(conLst)), str(len([i for i in conLst if i >= 3])), ','.join(resLst))
			yield resRec
			oldYear, curCon, conLst = curYear, 0, [0]
			if curEve == '1':
				curCon += 1
	resLst = [str(i) for i in conLst if i >= 1]
	resRec = '{0},{1},{2},{3}'.format(oldYear, str(max(conLst)), str(len([i for i in conLst if i >= 3])), ','.join(resLst))
	yield resRec

def genEveCnt(inCsv, outCsv):
	strLine = readDay(inCsv)
	for l in strLine:
		print >> outCsv, l 
		
if __name__ == "__main__": main()
