import sys, re, string, pickle
from itertools import *

def main():
	inCsv = open(sys.argv[1], 'r')
	outCsv = open(sys.argv[2], 'w')
	genEveCnt(inCsv, outCsv)
	inCsv.close()
	outCsv.close()

def readStr(inCsv):
	oldStr, oldSuf= '', ''
	lp=re.compile(r'(\d+),(\d+),(\d+),(.*)')
	eveCnt = ['0']*16
	for l in inCsv:
		m = lp.match(l)
		curStr=m.group(1)
		if oldStr == '':
			oldStr = curStr
			oldSuf = m.group(4)
		if curStr == oldStr:
			eveCnt[int(m.group(2))-1996]=m.group(3)
		else:
			resRec = '{0},{2},{1}'.format(oldStr, oldSuf, ','.join(eveCnt))
			yield resRec
			oldStr = curStr
			oldSuf = m.group(4)
			eveCnt = ['0']*16
			eveCnt[int(m.group(2))-1996]=m.group(3)
	resRec = '{0},{2},{1}'.format(oldStr, oldSuf, ','.join(eveCnt))
	yield resRec

def genEveCnt(inCsv, outCsv):
	strLine = readStr(inCsv)
	for l in strLine:
		print >> outCsv, l 
		
if __name__ == "__main__": main()
