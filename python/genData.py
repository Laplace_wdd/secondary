import sys, re, string, pickle
from itertools import *

def main():
	inCsv = open(sys.argv[1], 'r')
	valCsv = open(sys.argv[2], 'r')
	outCsv = open(sys.argv[3], 'w')
	nodeS1 = set()
	p = re.compile(',|(?:\s+)')
	for l in valCsv:
		nodeS1.add(l.strip())
	for l in inCsv:
		l = l.strip()
		la = p.split(l)
		if la[0] in nodeS1:
			for i in range(1,13):
				print >> outCsv, "{0},{1},{2}".format(la[0],la[1]+"-"+str(i),",".join(la[2:len(la)])) 
	inCsv.close()
	valCsv.close()
	outCsv.close()

if __name__ == "__main__": main()
