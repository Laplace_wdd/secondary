import sys, re, string, pickle
from itertools import *

def main():
	inCsv = open(sys.argv[1], 'r')
	outCsv = open(sys.argv[2], 'w')
	genEveCnt(inCsv, outCsv)
	inCsv.close()
	outCsv.close()

def readDay(inCsv):
	oldYear, curCon, conLst = '', 0, []
	for l in inCsv:
		entry = l.strip().split(",")
		curYear = entry[0]
		curEve = float(entry[1])
		if oldYear == '':
			oldYear = curYear
		if curYear == oldYear:
			if curEve > -10 and curEve < 10 :
				curCon += 1
			elif curCon != 0:
				conLst += [str(curCon)]
				curCon = 0
		else:
			if curCon != 0:
				conLst += [str(curCon)]
			resRec = '{0},{1}'.format(oldYear, ','.join(conLst))
			yield resRec
			oldYear, curCon, conLst = curYear, 0, []
			if curEve == '1':
				curCon += 1
	resRec = '{0},{1}'.format(oldYear, ','.join(conLst))
	yield resRec

def genEveCnt(inCsv, outCsv):
	strLine = readDay(inCsv)
	for l in strLine:
		print >> outCsv, l 
		
if __name__ == "__main__": main()
