import sys, re, string, pickle
from itertools import *

def main():
	inCsv = open(sys.argv[1], 'r')
	valCsv = open(sys.argv[2], 'r')
	outCsv = open(sys.argv[3], 'w')
	nodeS = set()
	p = re.compile(',|(?:\s+)')
	for l in valCsv:
		nodeS.add(l.strip())
	for l in inCsv:
		l = l.strip()
		la = p.split(l)
		if la[0] in nodeS:
			print >> outCsv, l
	inCsv.close()
	valCsv.close()
	outCsv.close()

if __name__ == "__main__": main()
