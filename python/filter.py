import sys, re, string, pickle
from itertools import *

def main():
	inCsv = open(sys.argv[1], 'r')
	valCsv = open(sys.argv[2], 'r')
	outCsv = open(sys.argv[3], 'w')
	newCsv = open(sys.argv[4], 'w')
	nodeS1, nodeS2 = set(), set()
	p = re.compile(',|(?:\s+)')
	for l in valCsv:
		nodeS1.add(l.strip())
	for l in inCsv:
		l = l.strip()
		la = p.split(l)
		if la[0] in nodeS2:
			print la[0]
			nodeS2.add(la[0])
		if la[0] in nodeS1:
			print >> outCsv, l
			print >> newCsv, la[0]
	inCsv.close()
	valCsv.close()
	outCsv.close()
	newCsv.close()

if __name__ == "__main__": main()
