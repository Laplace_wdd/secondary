import sys, re, string, pickle
from itertools import *

def main():
	inCsv1 = open(sys.argv[1], 'r')
	inCsv2 = open(sys.argv[2], 'r')
	outCsv = open(sys.argv[3], 'w')
	s1, s2 = set(), set()
	for l in inCsv1:
		s1.add(l.strip())
	for l in inCsv2:
		s2.add(l.strip())
	s1 |=s2
	print >> outCsv, "\n".join([i for i in s1])
	inCsv1.close()
	inCsv2.close()
	outCsv.close()

		
if __name__ == "__main__": main()
