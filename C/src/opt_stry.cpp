#include "opt_stry.h"

opt_stry::opt_stry(map<pair<string, string>, pair<double, double> > &p, map<pair<string, string>, double*> &f, int fn, int re)
{
	state_stry s(p,f,fn,re);
	this->s = s;
}

void opt_stry::run_loglin(double *coeff)
{
	    int ret = 0, N = this->s.fea_num;
		lbfgsfloatval_t fx;
		lbfgsfloatval_t *x = lbfgs_malloc(N);
		lbfgs_parameter_t param;
		lbfgs_parameter_init(&param);
		param.orthantwise_c=PENALTY;
		param.orthantwise_end=this->s.reg_end; 
		param.linesearch=LINESEARCH;
		for(int i = 0; i < N; ++ i) x[i] = coeff[i];
		ret = lbfgs(N, x, &fx, evaluate, progress, &s, &param);
		cout << "," << ret;
		cerr <<"Complete with code:"<< ret << endl;
		for (int i = 0; i < N; ++ i)
			coeff[i] = (double)x[i];
		lbfgs_free(x);
}

lbfgsfloatval_t opt_stry::evaluate(
    void *instance,
    const lbfgsfloatval_t *x,
    lbfgsfloatval_t *g,
    const int n,
    const lbfgsfloatval_t step
    )
{
	state_stry *s = (state_stry*) instance; 
    lbfgsfloatval_t fx = 0.0;
	int fean = s->fea_num;
	double* normal = new double[fean];
	double norm = 0;
	for (int i = 0; i < fean; ++ i) 
	{
		g[i] = 0.0;
		normal[i] = 0.0;
	}
	for (map<pair<string, string>, pair<double, double> >::iterator phi_itr = s->phi.begin(); phi_itr != s->phi.end(); ++ phi_itr)
	{
		double w1 = phi_itr->second.first;
		double w2 = phi_itr->second.second;
		norm += (w1 + w2); 
		double* fea = s->fea[phi_itr->first];
		double lin_comb = 0.0;
		for (int i = 0; i < fean; ++ i)
		{
			lin_comb += x[i]*fea[i];
			g[i] += w1*fea[i];
		}
		fx += w1*lin_comb - (w1 + w2)*log(1+exp(lin_comb));
		lin_comb = exp(lin_comb);
		for (int i = 0; i < fean; ++ i) normal[i] += (w1 + w2)*fea[i]*lin_comb/(1+lin_comb);
	}
	for (int i = 0; i < fean; ++ i) g[i] = (normal[i] - g[i])/norm;
	delete[] normal;
    return -fx/norm;
}

int opt_stry::progress(
    void *instance,
    const lbfgsfloatval_t *x,
    const lbfgsfloatval_t *g,
    const lbfgsfloatval_t fx,
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,
    int n,
    int k,
    int ls
    )
{
	fprintf(stderr, "Iteration %d:\n", k);
	cerr << "fx=" << fx << ",";
	fprintf(stderr, "xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
	return 0;
}
