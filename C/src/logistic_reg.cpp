#include "general.h"
#include "em_macro.h"
#include <lbfgs.h>
//#define TOTAL_FEATURE_NUM 24
//#define REG_END 24 
//#define TOTAL_FEATURE_NUM 63 
//#define REG_END 51 
//#define TOTAL_FEATURE_NUM 36 
//#define REG_END 24 
//#define TOTAL_FEATURE_NUM 75 
//#define REG_END 75 

set<string> s_lst, t_lst;
map<pair<string,string>,int> symap;
map<pair<string,string>, double*> stfea;
double* coeff = new double[TOTAL_FEATURE_NUM];
double ori = 0.5;

void init(int argc, const char* argv[])
{
	char s[MAX_STRLENGTH],y[MAX_STRLENGTH]; int e;
	for(int i = 0; i < TOTAL_FEATURE_NUM; ++ i) coeff[i] = ori;
	FILE* triple_in = fopen(argv[1], "r");
	while(!feof(triple_in))
	{
		fscanf(triple_in,"%s %s %d\n",s, y, &e);
		symap[make_pair(s,y)] = e;
	}
	fclose (triple_in);
	FILE* stf_in = fopen(argv[2], "r");
	while(!feof(stf_in))
	{
		fscanf(stf_in,"%s\n",y);
		double* f = new double[TOTAL_FEATURE_NUM];
		char* sid = strtok(y, ","); char* ptk = strtok (NULL, ",");
		char* t = ptk; ptk=strtok (NULL, ",");
		s_lst.insert(sid); t_lst.insert(t);
		int i = 0;
		while (ptk != NULL)
		{   
			f[i++] = atof(ptk);
			ptk = strtok (NULL, ",");
		}
		stfea[make_pair(sid,t)] = f;
	}
	fclose (stf_in);
}
lbfgsfloatval_t evaluate(												
    void *instance,																	
    const lbfgsfloatval_t *x,														
    lbfgsfloatval_t *g,																
    const int n,																	
    const lbfgsfloatval_t step														
    )																				
{																					
	int fea_num = TOTAL_FEATURE_NUM;														
    lbfgsfloatval_t fx = 0.0;														
	double normal = 0.0;															
	for (int i = 0; i < fea_num; ++ i) g[i] = 0.0;									
	set<string>::iterator s_itr;
	for (s_itr = s_lst.begin(); s_itr != s_lst.end(); ++ s_itr)									
	{																				
		string s = *s_itr;														
		set<string>::iterator t_itr;
		for (t_itr = t_lst.begin(); t_itr != t_lst.end(); ++ t_itr)								
		{																			
			string t = *t_itr;													
			double comb = 0.0;														
			double* fvec = stfea[make_pair(s,t)];									
			for (int k = 0; k < fea_num; ++ k) comb += fvec[k]*x[k];				
			double comb_exp = exp(comb), comb_sig = comb_exp/(1+comb_exp);			
			map<pair<string,string>,int>::iterator it = symap.find(make_pair(s,t));	
			if (it != symap.end())													
			{																		
				double w = it->second;									
				normal += it->second;														
				fx += w*(comb-log(1+exp(comb)));									
				for (int k = 0; k < fea_num; ++ k)									
				  g[k] += w*(1-comb_sig)*fvec[k];									
			}																		
			else																	
			{																		
				normal += 1;												
				fx -= log(1+exp(comb));									
				for (int k = 0; k < fea_num; ++ k)									
				  g[k] -= comb_sig*fvec[k];								
			}																		
		}																			
	}																				
	for (int k = 0; k < fea_num; ++ k) g[k] = -g[k]/normal;							
	return -fx/normal;
}
int progress(		
    void *instance,				
    const lbfgsfloatval_t *x,	
    const lbfgsfloatval_t *g,	
    const lbfgsfloatval_t fx,	
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,	
    int n,						
    int k,						
    int ls						
    )							
{								
    printf("Iteration %d:\n", k);
	cout << "fx=" << fx << ",";
//	for (int i = 0; i < FEATURE_NUM; ++ i)
//	  cout << "x[" << i << "] = " << x[i] << endl;
	 printf(" xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
//    printf("\n");
    return 0;					
}
void run_logistic()
{
	    int ret = 0, N = TOTAL_FEATURE_NUM;
		lbfgsfloatval_t fx;
		lbfgsfloatval_t *x = lbfgs_malloc(N);
		lbfgs_parameter_t param;
		lbfgs_parameter_init(&param);
		param.orthantwise_c=PENALTY/10;
		param.orthantwise_end=REG_END; 
		param.linesearch=LINESEARCH;
		param.epsilon=EPSILON;
		for(int i = 0; i < N; ++ i) x[i] = coeff[i];
		ret = lbfgs(N, x, &fx, evaluate, progress, NULL, &param);
		cout <<"ret="<< ret << '\n';
		for (int i = 0; i < N; ++ i) coeff[i] = (double)x[i];
		lbfgs_free(x);
}

void gen_model(const char* modelf)
{
	stringstream stf;
	FILE* stcoeff_out = fopen(modelf, "w");
	for (int j = 0; j < TOTAL_FEATURE_NUM; ++ j)
	  fprintf(stcoeff_out, "%f\n", coeff[j]);
	fclose(stcoeff_out);
}

void decon()
{
	set<string>::iterator s_itr;
	for (s_itr = s_lst.begin(); s_itr != s_lst.end(); ++ s_itr)									
	{																				
		string s = *s_itr;														
		set<string>::iterator t_itr;
		for (t_itr = t_lst.begin(); t_itr != t_lst.end(); ++ t_itr)								
		{																			
			string t = *t_itr;
			delete []stfea[make_pair(s,t)];
		}
	}
	delete []coeff;

}
int main(int argc, const char* argv[])
{
	init(argc, argv);
	run_logistic();
	gen_model(argv[3]);
	decon();
}
