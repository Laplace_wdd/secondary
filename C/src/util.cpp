#include"util.h"
namespace util{

	LINK_TYPE vector<string> tokenize(string target, const char* del){
		vector<string> res = vector<string>();
		int i = 0;
		string deli(del);
		bool flag = false;
		while(i < (int)target.length()){
			while(i < (int)target.length()&&deli.find(target[i]) != string::npos){
				if(!flag){
					flag = true;
					res.push_back("");
				}
				i++;
			}
			while(i < (int)target.length()&&deli.find(target[i]) == string::npos){
				flag = false;
				if(!res.size())res.push_back("");
				res.back() += target[i];
				i++;
			}
		}
		if(res.size() > 0 && res.back() == "")
			res.pop_back();
		return res;
	}

};
