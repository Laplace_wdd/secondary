#include "general.h"
#include <lbfgs.h>
class state_t
{
	public:
		map<string, double> phi;
		map<string, double*> fea;
		int fea_num, reg_end;
		state_t(){}
		state_t(map<string, double> &p, map<string, double*> &f, int fn, int re):phi(p), fea(f), fea_num(fn), reg_end(re){}
};

class opt_t 
{
	private:
		state_t s;
	public:
		opt_t(map<string, double> &p, map<string, double*> &f, int fn, int re);
		void run_loglin(double *coeff);
	protected:
		static lbfgsfloatval_t evaluate(
			void *instance,
			const lbfgsfloatval_t *x,
			lbfgsfloatval_t *g,
			const int n,
			const lbfgsfloatval_t step
			);
		static int progress(
			void *instance,
			const lbfgsfloatval_t *x,
			const lbfgsfloatval_t *g,
			const lbfgsfloatval_t fx,
			const lbfgsfloatval_t xnorm,
			const lbfgsfloatval_t gnorm,
			const lbfgsfloatval_t step,
			int n,
			int k,
			int ls
		);
};


