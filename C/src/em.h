#ifndef _EM_
#define _EM_
#include "general.h"
class em 
{
	private:
		int iteration;
		double likelihood();
		void E();
		void M();
		void gen_model(int);
	public:
		em(int, const char**);
		~em();
		void run();
};
#endif
