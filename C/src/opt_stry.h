#include "general.h"
#include <lbfgs.h>
class state_stry
{
	public:
		map<pair<string, string>, pair<double,double> > phi;
		map<pair<string, string>, double*> fea;
		int fea_num, reg_end;
		state_stry(){}
		state_stry(map<pair<string, string>, pair<double,double> > &p, map<pair<string, string>, double*> &f, int fn, int re):phi(p), fea(f), fea_num(fn), reg_end(re){}
};

class opt_stry 
{
	private:
		state_stry s;
	public:
		opt_stry(map<pair<string, string>, pair<double,double> > &p, map<pair<string, string>, double*> &f, int fn, int re);
		void run_loglin(double *coeff);
	protected:
		static lbfgsfloatval_t evaluate(
			void *instance,
			const lbfgsfloatval_t *x,
			lbfgsfloatval_t *g,
			const int n,
			const lbfgsfloatval_t step
			);
		static int progress(
			void *instance,
			const lbfgsfloatval_t *x,
			const lbfgsfloatval_t *g,
			const lbfgsfloatval_t fx,
			const lbfgsfloatval_t xnorm,
			const lbfgsfloatval_t gnorm,
			const lbfgsfloatval_t step,
			int n,
			int k,
			int ls
		);
};


