#include "general.h"
#include "em.h"

int main(int argc, const char* argv[])
{
	em my_em(argc, argv);
	my_em.run();
	return 0;
}
