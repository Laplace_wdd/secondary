#include "general.h"
#include "em_macro.h"
#include "em.h"

vector<string> valid_str_lst, inv_str_lst;
vector<string> year_lst;
map<pair<string,string>,int> symap;
map<string, pair<double,double> > alpha;
map<string,double> beta;
double ori = 0.5;
double lap = 0.001;
string dir;

LIKELIHOOD(beta)

em::em(int argc, const char* argv[])
{
	char s[20],y[20]; int e;
	FILE* triple_in = fopen(argv[1], "r");
	iteration=0;
	while(!feof(triple_in))
	{
		fscanf(triple_in,"%s %s %d\n",s, y, &e);
		symap[make_pair(s,y)] = e;
		alpha[s] = make_pair(ori,ori);
	}
	fclose (triple_in);
	FILE* str_in = fopen(argv[2], "r");
	while(!feof(str_in))
	{
		fscanf(str_in,"%s\n",s);
		if (alpha.find(s) != alpha.end())
			valid_str_lst.push_back(s);
		else
			inv_str_lst.push_back(s); 
	}
	fclose (str_in);
	FILE* year_in = fopen(argv[3], "r");
	while(!feof(year_in))
	{
		fscanf(year_in,"%s\n",y);
		year_lst.push_back(y);
		beta[y] = ori;
	}
	fclose (year_in);
	dir = argv[4];
}

E_STEP(beta)

M_STEP(
	d0+=lap;d1+=lap;d2+=lap;d3+=lap;,
	for (int j = 0; j < (int)year_lst.size(); ++ j)
	{
		string y = year_lst[j];
		double b = beta[y];
		b+=lap;b/=(N_max+2*lap);
		beta[y] = b;
	},
	beta
)

void em::gen_model(int i)
{
	stringstream cmd, strf, yf;
	cmd << "mkdir " << dir << "/model." << i; 
	system(cmd.str().c_str());
	map<string, pair<double,double> >::iterator alpha_itr;
	strf << dir << "/model." << i << "/str_model.csv";
	FILE* str_out = fopen(strf.str().c_str(), "w");
	for (alpha_itr = alpha.begin(); alpha_itr != alpha.end(); ++ alpha_itr)
		fprintf(str_out,"%s,%f,%f\n",alpha_itr->first.c_str(), alpha_itr->second.first, alpha_itr->second.second);
	fclose(str_out);
	yf << dir << "/model." << i << "/ym_model.csv";
	map<string,double>::iterator beta_itr;
	FILE* year_out = fopen(yf.str().c_str(), "w");
	for (beta_itr = beta.begin(); beta_itr != beta.end(); ++ beta_itr)
	  fprintf(year_out, "%s,%f\n", beta_itr->first.c_str(), beta_itr->second);
	fclose(year_out);
}

void em::run()
{
	RUN_EM(1)
}

em::~em(){}
