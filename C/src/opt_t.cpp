#include "opt_t.h"

opt_t::opt_t(map<string, double> &p, map<string, double*> &f, int fn, int re)
{
	state_t s(p,f,fn,re);
	this->s = s;
}

void opt_t::run_loglin(double *coeff)
{
	    int ret = 0, N = this->s.fea_num;
		lbfgsfloatval_t fx;
		lbfgsfloatval_t *x = lbfgs_malloc(N);
		lbfgs_parameter_t param;
		lbfgs_parameter_init(&param);
		param.orthantwise_c=TPENALTY;
		param.orthantwise_end=this->s.reg_end; 
		param.linesearch=LINESEARCH;
		for(int i = 0; i < N; ++ i) x[i] = coeff[i];
		ret = lbfgs(N, x, &fx, evaluate, progress, &s, &param);
		cout << "," << ret;
		for (int i = 0; i < N; ++ i)
			coeff[i] = (double)x[i];
		lbfgs_free(x);
}

lbfgsfloatval_t opt_t::evaluate(
    void *instance,
    const lbfgsfloatval_t *x,
    lbfgsfloatval_t *g,
    const int n,
    const lbfgsfloatval_t step
    )
{
	state_t *s = (state_t*) instance; 
    lbfgsfloatval_t fx = 0.0;
	int fean = s->fea_num;
	double* normal = new double[fean];
	for (int i = 0; i < fean; ++ i) 
	{
		g[i] = 0.0;
		normal[i] = 0.0;
	}
	for (map<string, double>::iterator phi_itr = s->phi.begin(); phi_itr != s->phi.end(); ++ phi_itr)
	{
		string y = phi_itr->first;
		double w = phi_itr->second;
		double* fea = s->fea[y];
		double lin_comb = 0.0;
		for (int i = 0; i < fean; ++ i)
		{
			lin_comb += x[i]*fea[i];
			g[i] += w*fea[i];
		}
		fx += w*lin_comb - log(1+exp(lin_comb));
		lin_comb = exp(lin_comb);
		for (int i = 0; i < fean; ++ i) normal[i] += fea[i]*lin_comb/(1+lin_comb);
	}
	for (int i = 0; i < fean; ++ i) g[i] = normal[i] - g[i];
	delete[] normal;
    return -fx;
}

int opt_t::progress(
    void *instance,
    const lbfgsfloatval_t *x,
    const lbfgsfloatval_t *g,
    const lbfgsfloatval_t fx,
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,
    int n,
    int k,
    int ls
    )
{
//	fprintf(stderr, "Iteration %d:\n", k);
//	cerr << "fx=" << fx << ",";
//	fprintf(stderr, "xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
    return 0;
}
