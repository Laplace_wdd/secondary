#include "general.h"
#include "opt_t.h"
#include "em_macro.h"
#include "em.h"

vector<string> valid_str_lst, inv_str_lst;
vector<string> year_lst;
map<pair<string,string>,int> symap;
map<string, pair<double,double> > alpha;
map<string, double> phi;
map<string, double*> fea;
double* coeff = new double[TFEATURE_NUM];
double ori = 0.5;
double lap = 0.001;
string dir;

LIKELIHOOD(phi)

em::em(int argc, const char* argv[])
{
	char s[MAX_STRLENGTH],y[MAX_STRLENGTH]; int e;
	FILE* triple_in = fopen(argv[1], "r");
	while(!feof(triple_in))
	{
		fscanf(triple_in,"%s %s %d\n",s, y, &e);
		symap[make_pair(s,y)] = e;
		alpha[s] = make_pair(ori,ori);
	}
	fclose (triple_in);
	FILE* str_in = fopen(argv[2], "r");
	while(!feof(str_in))
	{
		fscanf(str_in,"%s\n",s);
		if (alpha.find(s) != alpha.end())
			valid_str_lst.push_back(s);
		else
			inv_str_lst.push_back(s); 
	}
	fclose (str_in);
	FILE* year_in = fopen(argv[3], "r");
	for(int i = 0; i < TFEATURE_NUM; ++ i) coeff[i] = ori;
	while(!feof(year_in))
	{
		fscanf(year_in,"%s\n",y);
		double* f = new double[TFEATURE_NUM];
		char* ym = strtok(y, ","); char* ptk = strtok (NULL, ",");
		year_lst.push_back(ym);
		int i = 0; double pdct = 0.0;
		while (ptk != NULL)
		{   
			f[i] = atof(ptk);
			pdct += f[i]*coeff[i];
			ptk = strtok (NULL, ","); i++;
		}
		pdct=exp(pdct); phi[ym] = pdct/(1+pdct); fea[ym] = f;
	}
	fclose (year_in);
	dir = argv[4];
}

E_STEP(phi)

M_STEP(
	d0+=lap;d1+=lap;d2+=lap;d3+=lap;,
	for (int j = 0; j < (int)year_lst.size(); ++ j)
	{
		string y = year_lst[j];
		double b = phi[y];
		b/=N_max;
		phi[y] = b;
	}
	opt_t llt(phi, fea, TFEATURE_NUM, TREG_END);
	llt.run_loglin(coeff);
	for (int j = 0; j < (int)year_lst.size(); ++ j)
	{
		string y = year_lst[j]; double comb = 0.0;
		double* fvec = fea[y]; 
		for (int i = 0; i < TFEATURE_NUM; ++ i)
			comb += fvec[i]*coeff[i];	
		comb = exp(comb);
		phi[y] = comb/(1 + comb);
	}, 
	phi
)

void em::gen_model(int i)
{
	stringstream cmd, strf, yf, pf;
	cmd << "mkdir " << dir << "/model." << i; 
	system(cmd.str().c_str());
	map<string, pair<double,double> >::iterator alpha_itr;
	strf << dir << "/model." << i << "/alpha.csv";
	FILE* str_out = fopen(strf.str().c_str(), "w");
	for (alpha_itr = alpha.begin(); alpha_itr != alpha.end(); ++ alpha_itr)
		fprintf(str_out,"%s,%f,%f\n",alpha_itr->first.c_str(), alpha_itr->second.first, alpha_itr->second.second);
	fclose(str_out);
	yf << dir << "/model." << i << "/weights_phi.csv";
	FILE* coeff_out = fopen(yf.str().c_str(), "w");
	for (int j = 0; j < TFEATURE_NUM; ++ j)
	  fprintf(coeff_out, "%f\n", coeff[j]);
	fclose(coeff_out);
	pf << dir << "/model." << i << "/phi.csv";
	map<string,double>::iterator phi_itr;
	FILE* year_out = fopen(pf.str().c_str(), "w");
	for (phi_itr = phi.begin(); phi_itr != phi.end(); ++ phi_itr)
	  fprintf(year_out, "%s,%f\n", phi_itr->first.c_str(), phi_itr->second);
	fclose(year_out);
}

void em::run()
{
	RUN_EM(1)
}

em::~em()
{
	for (int i = 0; i < (int)year_lst.size(); ++ i) delete []fea[year_lst[i]];
	delete []coeff;

}
