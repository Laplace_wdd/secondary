#include "opt_st.h"

opt_st::opt_st(map<pair<string,string>,int> &sym,
					map<pair<string,string>, t_struct> &t,
					vector<string> &sl,
					vector<string> &tl,
					map<pair<string,string>, double*> &stf,
					int fn, int re){
	state_st s(sym, t, sl, tl, stf, fn, re);
	this->s = s;
}

void opt_st::run_loglin(double* coeff_p, double* coeff_m)
{
	    int ret = 0, N = this->s.fea_num;
		lbfgsfloatval_t fx;
		lbfgsfloatval_t *x = lbfgs_malloc(N);
		lbfgs_parameter_t param;
		lbfgs_parameter_init(&param);
		param.orthantwise_c=SPENALTY;
		param.orthantwise_end=this->s.reg_end; 
		param.linesearch=LINESEARCH;
		param.epsilon=EPSILON;
		//w+
		for(int i = 0; i < N; ++ i) x[i] = coeff_p[i];
		ret = lbfgs(N, x, &fx, evaluate_1, progress_1, &s, &param);
		cout << ',' << ret;
		cerr <<"w+ Complete with code:"<< ret << endl;
		for (int i = 0; i < N; ++ i) coeff_p[i] = (double)x[i];
		//w-
		for(int i = 0; i < N; ++ i) x[i] = coeff_m[i];
		ret = lbfgs(N, x, &fx, evaluate_0, progress_0, &s, &param);
		cout << ',' << ret;
		cerr <<"w- Complete with code:"<< ret << endl;
		for (int i = 0; i < N; ++ i) coeff_m[i] = (double)x[i];
		lbfgs_free(x);
}
#define LBFGS_EVAL(b)																\
lbfgsfloatval_t opt_st::evaluate_##b(												\
    void *instance,																	\
    const lbfgsfloatval_t *x,														\
    lbfgsfloatval_t *g,																\
    const int n,																	\
    const lbfgsfloatval_t step														\
    )																				\
{																					\
	state_st *s = (state_st*) instance;												\
	map<pair<string,string>,int> &symap = s->symap;									\
	map<pair<string,string>, t_struct> &tau = s->tau;								\
	vector<string> &s_lst = s->s_lst, &t_lst = s->t_lst;							\
	map<pair<string,string>, double*> &stfea = s->stfea;							\
	int fea_num = s->fea_num;														\
    lbfgsfloatval_t fx = 0.0;														\
	double normal = 0.0;															\
	for (int i = 0; i < fea_num; ++ i) g[i] = 0.0;									\
	for (int i = 0; i < (int)s_lst.size(); ++ i)									\
	{																				\
		string s = s_lst[i];														\
		for (int j = 0; j < (int)t_lst.size(); ++ j)								\
		{																			\
			string t = t_lst[j];													\
			double comb = 0.0;														\
			double* fvec = stfea[make_pair(s,t)];									\
			for (int k = 0; k < fea_num; ++ k) comb += fvec[k]*x[k];				\
			double comb_exp = exp(comb), comb_sig = comb_exp/(1+comb_exp);			\
			t_struct tu = tau[make_pair(s,t)];										\
			map<pair<string,string>,int>::iterator it = symap.find(make_pair(s,t));	\
			if (it != symap.end())													\
			{																		\
				double w = it->second*tu.t##b##1;									\
				normal += w;														\
				fx += w*(comb-log(1+exp(comb)));									\
				for (int k = 0; k < fea_num; ++ k)									\
				  g[k] += w*(1-comb_sig)*fvec[k];									\
			}																		\
			else																	\
			{																		\
				normal += tu.t##b##0;												\
				fx -= tu.t##b##0*log(1+exp(comb));									\
				for (int k = 0; k < fea_num; ++ k)									\
				  g[k] -= tu.t##b##0*comb_sig*fvec[k];								\
			}																		\
		}																			\
	}																				\
	for (int k = 0; k < fea_num; ++ k) g[k] = -g[k]/normal;							\
	return -fx/normal;																\
}																					\
																					\

#define LBFGS_PROG(b, info)															\
int opt_st::progress_##b(															\
    void *instance,																	\
    const lbfgsfloatval_t *x,														\
    const lbfgsfloatval_t *g,														\
    const lbfgsfloatval_t fx,														\
    const lbfgsfloatval_t xnorm,													\
    const lbfgsfloatval_t gnorm,													\
    const lbfgsfloatval_t step,														\
    int n,																			\
    int k,																			\
    int ls																			\
    )																				\
{																					\
	info																			\
    return 0;																		\
}
LBFGS_EVAL(1)
LBFGS_EVAL(0)
LBFGS_PROG(1, 
	fprintf(stderr, "Iteration %d:\n", k);
	cerr << "fx=" << fx << ",";
	fprintf(stderr, "xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
)
LBFGS_PROG(0, 
	fprintf(stderr, "Iteration %d:\n", k);
	cerr << "fx=" << fx << ",";
	fprintf(stderr, "xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
)
