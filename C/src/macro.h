#ifndef _MACRO_H
#define _MACRO_H
#define LINK_TYPE 
#define MAX_STRLENGTH 5000 
#define MONTH 12
#define YMB 
#ifdef YMB
	#define TFEATURE_NUM 64
#else
	#define TFEATURE_NUM 63 
#endif
//#define SFEATURE_NUM 46753 
//#define STRB
#ifdef STR
	#define SFEATURE_NUM 24
	#define EPSILON 1e-4
#else
	#define SFEATURE_NUM 25
	#define EPSILON 1e-5
#endif
#define TREG_END 51
#define SREG_END 24 
#define PENALTY 0.005
#define SPENALTY 0.0000005
#define TPENALTY 0.0015
#define LINESEARCH 2
#ifdef SYM
	#define TOTAL_FEATURE_NUM 87
	#define REG_END 75
#endif
#ifdef STR
	#define TOTAL_FEATURE_NUM 24 
	#define REG_END 24 
#endif
#ifdef SYMB
	#define TOTAL_FEATURE_NUM 88
        #define REG_END 75
#endif
#ifdef STRB
	#define TOTAL_FEATURE_NUM 25
	#define REG_END 24 
#endif
#ifdef SYMI
	#define TOTAL_FEATURE_NUM 36 
	#define REG_END 24 
#endif
#ifdef SYMNI
	#define TOTAL_FEATURE_NUM 75 
	#define REG_END 75 
#endif
struct t_struct 
{
	double t11, t10, t01, t00;
};
#endif
