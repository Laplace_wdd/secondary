#ifndef _EM_MACRO_H_
#define _EM_MACRO_H_
#define LIKELIHOOD(phi)										\
double em::likelihood()										\
{															\
	double res = 0.0;										\
	map<pair<string,string>,int>::iterator it;				\
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)	\
	{														\
		string s = valid_str_lst[i];						\
		pair<double, double> p = alpha[s];					\
		double ap = p.first, am = p.second;					\
		for (int j = 0; j < (int)year_lst.size(); ++ j)		\
		{													\
			string y = year_lst[j];							\
			double b = phi[y];								\
			it = symap.find(make_pair(s,y));				\
			if (it != symap.end())							\
			  res += log(ap*b+am*(1-b))*it->second;			\
			else											\
			  res += log(1-ap*b-am*(1-b));					\
		}													\
	}														\
	return res;												\
}															

#define E_STEP(phi)																\
map<pair<string,string>, t_struct> tau;											\
void em::E()																		\
{																				\
	tau.clear();																\
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)						\
	  for (int j = 0; j < (int)year_lst.size(); ++ j)							\
	  {																			\
		string s = valid_str_lst[i];											\
		string y = year_lst[j];													\
		pair<double, double> a = alpha[s];										\
		double ap = a.first, am = a.second, b = phi[y], cp = ap*b, cm = am*b;	\
		t_struct t;																\
		t.t11 = b==1?1:cp/(cp+am-cm);											\
		t.t10 = (b-cp)==(1-am+cm-cp)?1:(b-cp)/(1-am+cm-cp);						\
		t.t01 = 1-t.t11;														\
		t.t00 = 1-t.t10;														\
		tau[make_pair(s,y)] = t;												\
	  }																			\
}																				

#define M_STEP(smooth, update, phi)														\
void em::M()																		\
{																				\
	map<pair<string,string>,int>::iterator it;									\
	int N_s0 = inv_str_lst.size();												\
	map<string, int> N_s;														\
	for (int j = 0; j < (int)year_lst.size(); ++ j)								\
	{																			\
		phi[year_lst[j]] *= N_s0;												\
		N_s[year_lst[j]] = N_s0;												\
	}																			\
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)						\
	{																			\
		double d0 = 0.0, d1 = 0.0, d2 = 0.0, d3 = 0.0;							\
		string s = valid_str_lst[i];											\
		for (int j = 0; j < (int)year_lst.size(); ++ j)							\
		{																		\
			string y = year_lst[j];												\
			double b = phi[y];													\
			int cnt = N_s[y];													\
			t_struct t = tau[make_pair(s,y)];									\
			it = symap.find(make_pair(s,y));									\
			if (it != symap.end())												\
			{																	\
				d0 += t.t11*it->second;											\
				d2 += t.t01*it->second;											\
				b += t.t11*it->second;											\
				cnt += it->second;												\
			}																	\
			else																\
			{																	\
				d1 += t.t10;													\
				d3 += t.t00;													\
				b += t.t10;														\
				cnt += 1;														\
			}																	\
			phi[y] = b;															\
			N_s[y] = cnt;														\
		}																		\
		smooth;																	\
		double p = d0/(d0+d1), m = d2/(d2+d3);									\
		alpha[s] = make_pair(p, m);												\
	}																			\
	int N_max = 0;																\
	for (int j = 0; j < (int)year_lst.size(); ++ j)								\
	{																			\
		int cur_n = N_s[year_lst[j]];											\
		if(cur_n > N_max) N_max = cur_n;										\
	}																			\
	update																		\
}																				\

#define RUN_EM(condition)														\
	double llh = 0.0, ollh = 0.0;									\
	do																			\
	{																			\
		cout << ++iteration;\
		cerr << "EM Iteration:"<<iteration << endl;\
		E();																	\
		M();																	\
		ollh = llh;																\
		llh = likelihood();														\
		cout << "," << likelihood() << endl;								\
		cerr << "Likelihood:" << likelihood() << endl;								\
		gen_model(iteration);															\
	} while(condition);															

#endif
