#include <stdio.h>
#include <lbfgs.h>
#include <string.h>
#include <math.h>

static lbfgsfloatval_t evaluate(
    void *instance,
    const lbfgsfloatval_t *x,
    lbfgsfloatval_t *g,
    const int n,
    const lbfgsfloatval_t step
    )
{
    lbfgsfloatval_t fx = 3*x[0];
	double e = exp(fx);
	fx-=10*log(1+e);
	g[0] = -3+10*3*e/(1+e);
    return -fx;
}

static int progress(
    void *instance,
    const lbfgsfloatval_t *x,
    const lbfgsfloatval_t *g,
    const lbfgsfloatval_t fx,
    const lbfgsfloatval_t xnorm,
    const lbfgsfloatval_t gnorm,
    const lbfgsfloatval_t step,
    int n,
    int k,
    int ls
    )
{
    printf("Iteration %d:\n", k);
    printf("  fx = %f, x[0] = %f\n", fx, x[0]);
    printf("  xnorm = %f, gnorm = %f, step = %f\n", xnorm, gnorm, step);
    printf("\n");
    return 0;
}

#define N  1 

int main(int argc, char *argv[])
{
    int i, ret = 0;
    lbfgsfloatval_t fx;
    lbfgsfloatval_t *x = lbfgs_malloc(N);
    lbfgs_parameter_t param;

    if (x == NULL) {
        printf("ERROR: Failed to allocate a memory block for variables.\n");
        return 1;
    }

    /* Initialize the variables. */
    //for (i = 0;i < N; i ++ ) {
    //    x[i] = 0.001;
    //}

    /* Initialize the parameters for the L-BFGS optimization. */
    lbfgs_parameter_init(&param);
    /*param.linesearch = LBFGS_LINESEARCH_BACKTRACKING;*/

    /*
        Start the L-BFGS optimization; this will invoke the callback functions
        evaluate() and progress() when necessary.
     */
//	param.orthantwise_c = 1;
//	param.linesearch=2;
	param.orthantwise_end=1;
//	param.max_linesearch=1;
    ret = lbfgs(N, x, &fx, evaluate, progress, NULL, &param);

    /* Report the result. */
    printf("L-BFGS optimization terminated with status code = %d\n", ret);
    printf("  fx = %f, x[0] = %f\n", fx, x[0]);
	double a[N];
	for (int i = 0; i < N; ++ i) a[i] = x[i];
    printf("  fx = %f, a[0] = %f\n", fx, a[0]);
    lbfgs_free(x);
    return 0;
}
