#ifndef UTIL_H_
#define UTIL_H_
#include "macro.h"
#include "general.h"

namespace util{

	LINK_TYPE vector<string> tokenize(string target, const char* del);
};

#endif
