#include "general.h"

vector<string> valid_str_lst, inv_str_lst;
vector<int> year_lst;
map<pair<string,int>,int> symap;
map<string, pair<double,double> > alpha;
map<int,double> beta;
double ori = 0.001;

double likelihood()
{
	double res = 0.0;
	map<pair<string,int>,int>::iterator it;
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)
	{
		string s = valid_str_lst[i];
		pair<double, double> p = alpha[s];
		double ap = p.first, am = p.second;
		for (int j = 0; j < (int)year_lst.size(); ++ j)
		{
			int y = year_lst[j];
			double b = beta[y];
			it = symap.find(make_pair(s,y));
			if (it != symap.end())
			  res += log(ap*b+am*(1-b))*it->second;
			else
			  res += log(1-ap*b-am*(1-b));
		}
	}
	return res;
}
void init(int argc, const char* argv[])
{
	char s[10]; int y, e;
	FILE* triple_in = fopen(argv[1], "r");
	while(!feof(triple_in))
	{
		fscanf(triple_in,"%s %d %d\n",s, &y, &e);
		symap[make_pair(s,y)] = e;
		alpha[s] = make_pair(ori,ori);
	}
	fclose (triple_in);
	FILE* str_in = fopen(argv[2], "r");
	while(!feof(str_in))
	{
		fscanf(str_in,"%s\n",s);
		if (alpha.find(s) != alpha.end())
			valid_str_lst.push_back(s);
		else
			inv_str_lst.push_back(s); 
	}
	fclose (str_in);
	FILE* year_in = fopen(argv[3], "r");
	while(!feof(year_in))
	{
		fscanf(year_in,"%d\n",&y);
		year_lst.push_back(y);
		beta[y] = ori;
	}
	fclose (year_in);
}

struct t_struct 
{
	double t11, t10, t01, t00;
};
map<pair<string,int>, t_struct> tau;
void E()
{
	tau.clear();
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)
	  for (int j = 0; j < (int)year_lst.size(); ++ j)
	  {
		string s = valid_str_lst[i];
		int y = year_lst[j];
		pair<double, double> a = alpha[s];
		double ap = a.first, am = a.second, b = beta[y], cp = ap*b, cm = am*b;
		t_struct t;
		t.t11 = b==1?1:cp/(cp+am-cm);
		t.t10 = (b-cm)==(1-am+cm-cp)?1:(b-cm)/(1-am+cm-cp);
		t.t01 = 1-t.t11;
		t.t00 = 1-t.t10;
		tau[make_pair(s,y)] = t;
	  }
}

void M()
{
	map<pair<string,int>,int>::iterator it;
	int N_s0 = inv_str_lst.size(); 
	int N_s = N_s0 + valid_str_lst.size();
	for (int j = 0; j < (int)year_lst.size(); ++ j)
		beta[year_lst[j]] *= N_s0;
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)
	{
		double d0 = 0.0, d1 = 0.0, d2 = 0.0, d3 = 0.0;
		string s = valid_str_lst[i];
		for (int j = 0; j < (int)year_lst.size(); ++ j)
		{
			int y = year_lst[j];
			double b = beta[y];
			t_struct t = tau[make_pair(s,y)];
			it = symap.find(make_pair(s,y));
			if (it != symap.end())
			{
				d0 += t.t11*it->second;
				d2 += t.t01*it->second;
				b += t.t11*it->second;
			}
			else
			{
				d1 += t.t10;
				d3 += t.t00;
				b += t.t00;
			}
			beta[y] = b;
		}
		double p = d0/(d0+d1), m = d2/(d2+d3);
		alpha[s] = make_pair(p, m);
	}
	for (int j = 0; j < (int)year_lst.size(); ++ j)
		beta[year_lst[j]] /= N_s;
}

void gen_model(int argc, const char* argv[])
{
	map<string, pair<double,double> >::iterator alpha_itr;
	FILE* str_out = fopen(argv[4], "w");
	for (alpha_itr = alpha.begin(); alpha_itr != alpha.end(); ++ alpha_itr)
		fprintf(str_out,"%s,%f,%f\n",alpha_itr->first.c_str(), alpha_itr->second.first, alpha_itr->second.second);
	fclose(str_out);

	map<int,double>::iterator beta_itr;
	FILE* year_out = fopen(argv[5], "w");
	for (beta_itr = beta.begin(); beta_itr != beta.end(); ++ beta_itr)
	  fprintf(year_out, "%d,%f\n", beta_itr->first, beta_itr->second);
	fclose(year_out);
}

int main(int argc, const char* argv[])
{
	init(argc, argv);
	for (int i = 0; i < 100; ++ i)
	{
		cout << i <<"," << likelihood() << endl;
		E();
		M();
	}
	gen_model(argc, argv);
	return 0;
}
