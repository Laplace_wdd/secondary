#include "general.h"

vector<string> valid_str_lst, inv_str_lst;
vector<int> year_lst;
map<pair<string,int>,int> symap;
map<string, pair<double,double> > alpha;
map<int,double> beta;

void init(int argc, const char* argv[])
{
	char s[10]; int y, e;
	FILE* triple_in = fopen(argv[1], "r");
	while(!feof(triple_in))
	{
		fscanf(triple_in,"%s %d %d\n",s, &y, &e);
		symap[make_pair(s,y)] = e;
		alpha[s] = make_pair(1.0,1.0);
	}
	fclose (triple_in);
	FILE* str_in = fopen(argv[2], "r");
	while(!feof(str_in))
	{
		fscanf(str_in,"%s\n",s);
		if (alpha.find(s) != alpha.end())
			valid_str_lst.push_back(s);
		else
			inv_str_lst.push_back(s); 
	}
	fclose (str_in);
	FILE* year_in = fopen(argv[3], "r");
	while(!feof(year_in))
	{
		fscanf(year_in,"%d\n",&y);
		year_lst.push_back(y);
		beta[y] = 1.0;
	}
	fclose (year_in);
}

struct t_struct 
{
	double t11, t10, t01, t00;
	t_struct(double t11, double t10, double t01, double t00)
	{
		this->t11 = t11;
		this->t10 = t10;
		this->t01 = t01;
		this->t00 = t00;
	}
};
map<pair<string,int>, t_struct> tau;
void E()
{
	tau.clear();
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)
	  for (int j = 0; j < (int)year_lst.size(); ++ j)
	  {
		string s = valid_str_lst[i];
		int y = year_lst[j];
		pair<double, double> a = alpha[s];
		double ap = a.first, am = a.second, b = beta[y], cp = ap*b, cm = am*b;
		double t11 = cp/(cp+am-cm);
		double t10 = (b-cm)/(1-am+cm-cp);
		double t01 = 1-t11;
		double t00 = 1-t10;
		tau[make_pair(s,y)] = t_struct(t11, t10, t01, t00);
	  }
}

void M()
{
	map<pair<string,int>,int>::iterator it;
	int N_s0 = inv_str_lst.size(); 
	int N_s = N_s0 + valid_str_lst.size();
	for (int j = 0; j < (int)year_lst.size(); ++ j)
		beta[year_lst[j]] *= N_s0;
	for (int i = 0; i < (int)valid_str_lst.size(); ++ i)
	{
		double d0 = 0.0, d1 = 0.0, d2 = 0.0, d3 = 0.0;
		string s = valid_str_lst[i];
		for (int j = 0; j < (int)year_lst.size(); ++ j)
		{
			int y = year_lst[j];
			double* b = &(beta[y]);
			t_struct t = tau[make_pair(s,y)];
			cout << s << ":" << y << ":" << t.t11 << endl;
			it = symap.find(make_pair(s,y));
			if (it != symap.end())
			{
				d0 += t.t11*it->second;
				d2 += t.t01*it->second;
				*b += t.t11*it->second;
			}
			else
			{
				d1 += t.t10;
				d2 += t.t00;
				*b += t.t00;
			}
		}
		pair<double,double>* p = &alpha[s];
		p->first = d0/(d0+d1);
		p->second = d2/(d2+d3);
	}
	for (int j = 0; j < (int)year_lst.size(); ++ j)
		beta[year_lst[j]] /= N_s;

}

int main(int argc, const char* argv[])
{
	init(argc, argv);
	for (int i = 0; i < 1; ++ i)
	{
		E();
		M();
	}
//	for (int j = 0; j < year_lst.size(); ++ j)
//	  cout << year_lst[j] << ":" <<beta[year_lst[j]] << endl;
	return 0;
}
