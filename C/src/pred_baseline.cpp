#include "general.h"
//#define TOTAL_FEATURE_NUM 87
//#define TOTAL_FEATURE_NUM 24
//#define TOTAL_FEATURE_NUM 63 
//#define TOTAL_FEATURE_NUM 36 
//#define TOTAL_FEATURE_NUM 75 

double* coeff = new double[TOTAL_FEATURE_NUM];
map<string, double> str2score;
map<pair<string, string>, double> sym2score;

void init(int argc, const char* argv[])
{
	char y[MAX_STRLENGTH];
	FILE* model_in = fopen(argv[2], "r");
	int i = 0;
	while(!feof(model_in))
		fscanf(model_in,"%lf\n",&coeff[i++]);
	fclose(model_in);
	FILE* stf_in = fopen(argv[1], "r");
	while(!feof(stf_in))
	{
		fscanf(stf_in,"%s\n",y);
		char* sid = strtok(y, ","); char* ptk = strtok (NULL, ",");
		char* ym = ptk; ptk=strtok (NULL, ",");
		int i = 0; double pdct = 0.0;
		while (ptk != NULL)
		{
			pdct += atof(ptk)*coeff[i++];
			ptk = strtok(NULL, ",");
		}
		pdct=exp(pdct); pdct = pdct/(1+pdct);
		str2score[sid]+=pdct;
		sym2score[make_pair(sid, ym)] = pdct;
	}
	fclose(stf_in);
}

void gen_res(const char* out_name)
{
	stringstream out_by_year, out_by_month;
	out_by_year << out_name << ".year";
	FILE* outyf = fopen(out_by_year.str().c_str(), "w");
	map<string, double>::iterator itry;
	for (itry = str2score.begin(); itry != str2score.end(); ++ itry)
		fprintf(outyf, "%s,%lf\n", itry->first.c_str(), itry->second);
	fclose(outyf);
	out_by_month << out_name << ".month";
	FILE* outmf = fopen(out_by_month.str().c_str(), "w");
	map<pair<string, string>, double>::iterator itrm;
	for (itrm = sym2score.begin(); itrm != sym2score.end(); ++ itrm)
		fprintf(outmf, "%s,%s,%lf\n", itrm->first.first.c_str(), itrm->first.second.c_str(), itrm->second);
	fclose(outmf);

}

void decon()
{
	delete[] coeff;
}
int main(int argc, const char* argv[])
{
	init(argc, argv);
	gen_res(argv[3]);
	decon();
}
