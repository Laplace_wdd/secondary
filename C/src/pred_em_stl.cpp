#include "general.h"

map<string, double> t2score, s2score;
map<pair<string, string>, double> sym2score;
double* tcoeff = new double[TFEATURE_NUM];
double* stcoeff_p = new double[SFEATURE_NUM];
double* stcoeff_m = new double[SFEATURE_NUM];

void init(int argc, const char* argv[])
{
	stringstream wss, wts;
	wss << argv[3] << "/weights_stphi.csv";
	FILE* smodel_in = fopen(wss.str().c_str(), "r");
	int i = 0;
	while(!feof(smodel_in))
	{
		fscanf(smodel_in,"%lf%*c%lf\n", &stcoeff_p[i], &stcoeff_m[i]);
		i++;
	}
	fclose(smodel_in);
	wts << argv[3] << "/weights_tphi.csv";
	FILE* tmodel_in = fopen(wts.str().c_str(), "r");
	i = 0;
	while(!feof(tmodel_in))
		fscanf(tmodel_in,"%lf\n",&tcoeff[i++]);
	fclose(tmodel_in);
	char y[MAX_STRLENGTH];
	FILE* tf_in = fopen(argv[1], "r");
	while(!feof(tf_in))
	{
		fscanf(tf_in,"%s\n",y);
		char* t = strtok(y, ","); char* ptk = strtok (NULL, ",");
		int i = 0; double pdct = 0.0;
		while (ptk != NULL)
		{
			pdct += atof(ptk)*tcoeff[i++];
			ptk = strtok(NULL, ",");
		}
		pdct=exp(pdct); pdct = pdct/(1+pdct);
		t2score[t]+=pdct;
	}
	FILE* stf_in = fopen(argv[2], "r");
	while(!feof(stf_in))
	{
		fscanf(stf_in,"%s\n",y);
		char* sid = strtok(y, ","); char* ptk = strtok (NULL, ",");
		char* ym = ptk; ptk=strtok (NULL, ",");
		int i = 0; double pdctp = 0.0, pdctm = 0.0;
		while (ptk != NULL)
		{
			pdctp += atof(ptk)*stcoeff_p[i];
			pdctm += atof(ptk)*stcoeff_m[i++];
			ptk = strtok(NULL, ",");
		}
		pdctp=exp(pdctp); pdctp = pdctp/(1+pdctp);
		pdctm=exp(pdctm); pdctm = pdctm/(1+pdctm);
		double tprob=t2score[ym];
		double score = pdctp*tprob+pdctm*(1-tprob); 
		s2score[sid]+=score;
		sym2score[make_pair(sid, ym)] = score;
	}
	fclose(stf_in);
}

void gen_res(const char* out_name)
{
	stringstream out_by_year, out_by_month;
	out_by_year << out_name << ".year.csv";
	FILE* outyf = fopen(out_by_year.str().c_str(), "w");
	map<string, double>::iterator itry;
	for (itry = s2score.begin(); itry != s2score.end(); ++ itry)
		fprintf(outyf, "%s,%lf\n", itry->first.c_str(), itry->second);
	fclose(outyf);
	out_by_month << out_name << ".month.csv";
	FILE* outmf = fopen(out_by_month.str().c_str(), "w");
	map<pair<string, string>, double>::iterator itrm;
	for (itrm = sym2score.begin(); itrm != sym2score.end(); ++ itrm)
		fprintf(outmf, "%s,%s,%lf\n", itrm->first.first.c_str(), itrm->first.second.c_str(), itrm->second);
	fclose(outmf);
}

void decon()
{
	delete[] tcoeff; 
	delete[] stcoeff_p;
	delete[] stcoeff_m;
}

int main(int argc, const char* argv[])
{
	init(argc, argv);
	gen_res(argv[4]);
	decon();
}
