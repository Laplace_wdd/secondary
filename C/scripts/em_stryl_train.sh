export LD_LIBRARY_PATH="/export/home/dw2546/lib"
data=train_stryl${train_year}${xi}
#data_path=/export/projects/nlp/nlp4fe/dw2546/secondary/workspace
data_path=/localtemp/dw2546/secondary/workspace
run_path=/export/home/dw2546/secondary/code/C
local_path=${data_path}/em_stryl/${data}
if [ ! -d ${local_path} ]
then
        mkdir ${local_path}
fi
${run_path}/em_stryl_c${xi} ${data_path}/input/${train_year}/s2ym ${data_path}/input/raw_data/str_lst ${data_path}/input/${train_year}/stry_feature ${data_path}/input/${train_year}/ymb_feature ${local_path} ${data_path}/em_tl/model.3968/weights_phi.csv >> ${local_path}/likelihood.csv 
