#!/bin/sh
eventdata=/localtemp/dw2546/secondary/workspace/input
scoredir=/export/home/dw2546/secondary/code/C/pred_old
#for train in 2000 2001 2002 2003 2004 2005 2006 2007 2008 2009
#do
#	for test in $(echo ${train}+1|bc) 
#	do
#		echo ${train}_${test}
#		for tar in symb strb
#		do
#			python ${eventdata}/scripts/genlikelihood.py ${eventdata}/${test}/s2ym ${scoredir}/baseline_train${train}test${test}${tar}.csv.month 
#		done
#		echo ""
#	done
#done
#python ${eventdata}/scripts/genlikelihood.py ${eventdata}/2010/s2ym ${scoredir}/em_stl_model4300train2009test2010.csv.month.csv
#python ${eventdata}/scripts/genlikelihood.py ${eventdata}/2010/s2ym ${scoredir}/baseline_train2000_2009test2010strb.csv.month 
for model in 322 566 690 907 1270 1580 1750
do
	echo ${model}
	python ${eventdata}/scripts/genlikelihood.py ${eventdata}/2010/s2ym ${scoredir}/em_stl_model${model}train2000_2009test2010.csv.month.csv
done
