export LD_LIBRARY_PATH="/export/home/dw2546/lib"
train_year=$1
test_year=$2
data=train${train_year}test${test_year}
#data_path=/export/projects/nlp/nlp4fe/dw2546/secondary/workspace
data_path=/localtemp/dw2546/secondary/workspace
run_path=/export/home/dw2546/secondary/code/C
model=$3
${run_path}/pred_em_stl ${data_path}/input/${test_year}/ymb_feature ${data_path}/input/${test_year}/strb_feature ${data_path}/em_stl/train${train_year}/model.${model} ${run_path}/pred/em_stl_model${model}${data}.csv 
