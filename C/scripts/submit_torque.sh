#!/bin/sh
export LD_LIBRARY_PATH="/export/home/dw2546/lib"
for TRAIN_YEAR in 2009 2008 2007 2006 2005 2004 2003 2002 2001 2000 
do
	for TEST_YEAR in $(echo ${TRAIN_YEAR}+1|bc) 
	do
		echo ${TRAIN_YEAR}_${TEST_YEAR}
		for TAR in symb #sym symi strb str
		do
			export train_year=${TRAIN_YEAR}
			export test_year=${TEST_YEAR}
			export tar=${TAR}
		#	qsub -V baseline_core.sh  
			./baseline_core.sh  
		done
	done
done
exit 0
