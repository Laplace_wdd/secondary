export LD_LIBRARY_PATH="/export/home/dw2546/lib"
train_year=2000_2008
dev_year=2009
test_year=2010
xi=$1
dev_data=train${train_year}test${dev_year}${xi}
test_data=train${train_year}test${test_year}${xi}
eventdata=/localtemp/dw2546/secondary/workspace/input
#data_path=/export/projects/nlp/nlp4fe/dw2546/secondary/workspace
data_path=/localtemp/dw2546/secondary/workspace
run_path=/export/home/dw2546/secondary/code/C
model=$2
model_dir=${data_path}/em_stryl/train_stryl${train_year}${xi}/model.${model}
echo ${model_dir}
${run_path}/pred_em_stl ${data_path}/input/${dev_year}/ymb_feature ${data_path}/input/${dev_year}/strb_feature ${model_dir} ${data_path}/pred/em_stryl_model${model}${dev_data}.csv 
dev_res=$(python ${eventdata}/scripts/genlikelihood.py ${eventdata}/${dev_year}/s2ym ${data_path}/pred/em_stryl_model${model}${dev_data}.csv.month.csv)
${run_path}/pred_em_stl ${data_path}/input/${test_year}/ymb_feature ${data_path}/input/${test_year}/strb_feature ${model_dir} ${data_path}/pred/em_stryl_model${model}${test_data}.csv 
test_res=$(python ${eventdata}/scripts/genlikelihood.py ${eventdata}/${test_year}/s2ym ${data_path}/pred/em_stryl_model${model}${test_data}.csv.month.csv)
echo ${model},${dev_res},${test_res}
